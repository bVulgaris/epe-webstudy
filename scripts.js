var questions = [];
function randomize3(){
  
  $(".question").each(function( index ) {
      questions.push($(this));
  });

  $(".data").html("");

  questions = questions.sort(function() {return Math.random() - 0.5});

  for (var i = 0; i < questions.length; i++) {
      var cosa = "<ul class='question'>"+questions[i].html()+"</ul>";
      $(".data").append(cosa);
   }

  addEvent();
}
/******************************************/
function randomize2(){
  var questions = [];
  $(".question").each(function( index ) {
      questions.push($(this));
  });

  $(".data").html("");
  var indexList = [];
  while(indexList.length != questions.length){
      var index = Math.floor(Math.random()*questions.length);
      if(indexList.indexOf(index)==-1){
        var question = questions[Math.floor(Math.random()*questions.length)];
        var allQuestion = "<ul class='question'>"+question.html()+"</ul>";
        $(".data").append(allQuestion);
      }
      indexList.push(index);
  }
  addEvent();
}
/******************************************/
function randomize() {

  console.log("neWRandomize");
  
  $(".question").each(function( index ) {
    questions.push($(this));
  });

  var currentIndex = questions.length, temporaryValue, randomIndex;

  while (0 !== currentIndex) {

    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    temporaryValue = questions[currentIndex];
    questions[currentIndex] = questions[randomIndex];
    questions[randomIndex] = temporaryValue;
  }
  $(".data").html("");
  for (var i = 0; i < questions.length; i++) {
    var cosa = "<ul class='question'>"+questions[i].html()+"</ul>";
    $(".data").append(cosa);
  }

  addEvent();
}
/******************************************/

function isRepeat(a) {
  for(var i = 0; i <= a.length; i++) {
    for(var j = i; j <= a.length; j++) {
        if(i != j && a[i] == a[j]) {
            return true;
        }
    }
}
return false;
}


function buscar(){
  if ($("#input").val()=="") {
    $(".data").show();
  }
};
function addEvent(){
    $( ".erantzuna" ).click(function(){
      if($(this).hasClass("ondo")||$(this).hasClass("txarto")){
        $(this).removeClass("ondo");
        $(this).removeClass("txarto");
      }else{
        if($(this).hasClass("zuzena")){
          $(this).addClass("ondo");
        }else{
          $(this).addClass("txarto");
        }
      }
      $("#ok").html($( ".ondo" ).length-$( ".txarto" ).length);
      $("#fail").html($( ".txarto" ).length);
    });
  }

function save() {
  console.log("Save!");
  localStorage.setItem("html", document.body.innerHTML);
}
function clearize() {
  console.log("clear!");
  localStorage.clear();
}
function okFilter(){
  var galderak = [];
  $(".galdera").each(function( index ){
    if(index!=109){
      galderak.push($(this));
    }
  });
  var erantzunak = [];
  $(".zuzena").each(function( index ) {
      erantzunak.push($(this));
  });

  $(".data").html("");

  for (var i = 0; i < erantzunak.length; i++) {
    var minified = "<p>"+galderak[i].html()+"</p>"+"<ul><li>"+erantzunak[i].html()+"</li></ul></br>";
    $(".data").append(minified);
  }
}

/****************************************/

$( document ).ready(function() {
  let content = localStorage.getItem("html");
  if(content) {
    document.body.innerHTML = content
  }
  addEvent();
});
